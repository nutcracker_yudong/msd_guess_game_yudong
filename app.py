from flask import Flask, jsonify, request, session
from random import *

app = Flask(__name__)
app.secret_key = 'You Will Never Guess'


@app.route("/")
def welcome():
    return "Welcome to the guess game! <br /><br />/start to start a game <br /><br />/guess?value=10 to guess a number"


@app.route("/start")
def start():
    session['target'] = randint(1, 100)
    print("the secret number is " + str(session['target']))
    return "re-seed the number to be guessed "


@app.route("/guess")
def guess():
    value = int(request.args.get('value'))
    if session.get('target') is None:
        return "you should start before guess"

    target = session['target']
    print(str(value) + " " + str(target))
    return jsonify({
        "status": "+" if value > target else "-" if value < target else "=",
        "message": "Your guess is " + "too high" if value > target else "too low" if value < target else "correct!",
    })


if __name__ == "__main__":
    app.run(debug=True)
